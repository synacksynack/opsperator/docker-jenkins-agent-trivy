FROM docker.io/openshift/jenkins-slave-base-centos7:v3.11

ENV TRIVY_VERSION=0.28.0

LABEL com.redhat.component="jenkins-agent-trivy-centos7-container" \
      io.k8s.description="The jenkins agent trivy image has the tools scanning Docker images against Trivy Server or Standalone." \
      io.k8s.display-name="Jenkins Agent - Trivy" \
      io.openshift.tags="openshift,jenkins,agent,trivy" \
      architecture="x86_64" \
      name="ci/jenkins-agent-trivy" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-jenkins-agent-trivy" \
      version="1.0.4"

RUN set -x \
    && if uname -m | grep aarch64 >/dev/null; then \
	curl -o /usr/src/trivy.rpm -fsL \
	    https://github.com/aquasecurity/trivy/releases/download/v$TRIVY_VERSION/trivy_${TRIVY_VERSION}_Linux-ARM64.rpm; \
    else \
	curl -o /usr/src/trivy.rpm -fsL \
	    https://github.com/aquasecurity/trivy/releases/download/v$TRIVY_VERSION/trivy_${TRIVY_VERSION}_Linux-64bit.rpm; \
    fi \
    && yum -y localinstall /usr/src/trivy.rpm \
    && if yum -y install epel-release; then \
	if test "$DO_UPGRADE"; then \
	    yum -y upgrade; \
	fi \
	&& yum -y install ca-certificates jq \
	&& yum clean all -y; \
    else \
	curl https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64 -o /usr/bin/jq \
	&& chmod +x /usr/bin/jq; \
    fi

USER 1001
